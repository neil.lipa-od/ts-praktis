"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Truck = void 0;
class Truck {
    constructor(_cargo) {
        this._cargo = _cargo;
    }
    add(basket) {
        this._cargo.push(basket);
    }
    listOwners() {
        let array = [];
        this._cargo.forEach((basket) => array.push(basket.owner));
        return array;
    }
    listFruits() {
        return this._cargo.map((basket) => basket.fruits).flat().map((element) => element[0]);
    }
    listSortedFruits() {
        return this._cargo.map((basket) => basket.fruits).flat().sort((a, b) => a[1] - b[1]).map((element) => element[0]);
    }
}
exports.Truck = Truck;
