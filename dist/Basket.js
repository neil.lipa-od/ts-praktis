"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Basket = void 0;
class Basket {
    constructor(_owner, _material, _fruits) {
        this._owner = _owner;
        this._material = _material;
        this._fruits = _fruits;
    }
    get owner() {
        return this._owner;
    }
    get material() {
        return this._material;
    }
    get fruits() {
        return this._fruits;
    }
}
exports.Basket = Basket;
