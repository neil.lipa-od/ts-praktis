import { Basket } from "./Basket";

export class Truck {
  constructor(private _cargo: Basket[]) { }
  
  add(basket: Basket): void {
    this._cargo.push(basket);
  }
  listOwners(): string[] {
    let array = [];
    this._cargo.forEach((basket) => array.push(basket.owner));
    return array;
  }
  listFruits() {
    return this._cargo.map((basket) => basket.fruits).flat().map((element) => element[0]);
  }
  listSortedFruits() {
    return this._cargo.map((basket) => basket.fruits).flat().sort((a, b) => a[1] - b[1]).map((element) => element[0]);
  }
}