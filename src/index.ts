import { Basket } from './Basket';
import { Truck } from './Truck';


const basket1 = new Basket('Philip', 'abaca', [['papaya', 3]]);
const basket2 = new Basket('Wendy', 'abaca', [['apple', 3], ['grapes', 20], ['melon', 1]]);
const basket3 = new Basket('Cindy', 'plastic', [['apple', 1], ['papaya', 2], ['watermelon', 1]]);
const basket4 = new Basket('Michelle', 'wood', [['apple', 7], ['grapes', 30]]);

let t: Truck = new Truck([]);
t.add(basket1);
// console.log(t);
t.add(basket2);
t.add(basket3);
t.add(basket4);
console.log(t.listSortedFruits());
