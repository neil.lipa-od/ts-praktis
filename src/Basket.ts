export type Material = 'abaca' | 'plastic' | 'wood' | 'fabric';
export type Fruits = 'apple' | 'grapes' | 'melon' | 'watermelon' | 'papaya' ;
export class Basket {
  constructor(private _owner: string, private _material: Material, private _fruits: [Fruits, number][]) { }
  get owner(): string {
    return this._owner;
  }
  get material(): Material {
    return this._material;
  }
  get fruits(): [Fruits, number][] {
    return this._fruits;
  }
}